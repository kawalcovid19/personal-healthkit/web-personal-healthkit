<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Seller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProductController extends Controller
{
    public function __construct()
    {
        //$this->middleware("loginSeller");
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required',
            'ingredients' => 'required',
            'requirements' => 'required',
            'description' => 'required',
            'url' => 'required',
            'stock' => 'required',
            'ableToProduce' => 'required',
            'image' => 'required'
            
        ]);
        $seller =  Seller::where('token', $request->input('token'))->first();
        //dd($request);
        $product = Product::create(array_merge($request->all(), ['seller_id' => $seller->id]));
 
        return response()->json($product);
   
    }

    public function index()
    {
        return response()->json(Product::paginate(10));

        //return Product::all();
    }

    public function show($id)
    {
        $product = Product::find($id);
        if (! $product) {
            return response()->json([
               'message' => 'product not found'
            ]);
        }

        return $product;
    }

    public function update(Request $request, $id)
    {
        $product = Product::find($id);

        if ($product) {
            $product->update($request->all());

            return response()->json([
                'message' => 'Product has been updated'
            ]);
        }

        return response()->json([
            'message' => 'Product not found',
        ], 404);
    }

    public function delete($id)
    {
        $product = Product::find($id);

        if ($product) {
            $product->delete();

            return response()->json([
               'message' => 'Product has been deleted'
            ]);
        }

        return response()->json([
            'message' => 'Product not found'
        ], 404);
    }

}
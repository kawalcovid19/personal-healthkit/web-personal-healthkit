<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Seller;
use App\Models\Admin;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct()
    {
        // $this->middleware("loginUser");
    }

    public function validateProduct(Request $request, $id)
    {
        $product = Product::find($id);
        $product->isValidate = 1;
        if ($product) {
            $product->update($request->all());

            return response()->json([
                'message' => 'Product Validated'
            ]);
        }

        return response()->json([
            'message' => 'Product not found',
        ], 404);
    }

    public function rejectProduct(Request $request, $id)
    {
        $product = Product::find($id);
        $product->isValidate = 2;
        if ($product) {
            $product->update($request->all());

            return response()->json([
                'message' => 'Product Rejected'
            ]);
        }

        return response()->json([
            'message' => 'Product not found',
        ], 404);
    }

    

}
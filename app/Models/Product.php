<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = ['name', 'image','price', 'description', 'isValidate', 'requirements', 'ingredients','seller_id', 'url', 'stock', 'ableToProduce'];

    public function seller()
    {
        return $this->belongsTo(Seller::class);
    }

}
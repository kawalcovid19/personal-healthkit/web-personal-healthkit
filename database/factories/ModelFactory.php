<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'email' => 'user@user.com',//$faker->email,
        'password' => Hash::make('12345678'),
        'name' => $faker->name,
        'phone' => $faker->e164PhoneNumber,
        'street' => $faker->streetAddress,
        'city' => $faker->city,
    ];
});

$factory->define(App\Models\Admin::class, function (Faker $faker) {
    return [
        'email' => 'admin@admin.com',
        'password' => Hash::make('12345678'),
        'name' => $faker->name,
        'phone' => $faker->e164PhoneNumber,
    ];
});

$factory->define(App\Models\Seller::class, function (Faker $faker) {
    return [
        'email' => 'seller@seller.com',
        'password' => Hash::make('12345678'),
        'name' => $faker->name,
        'phone' => $faker->e164PhoneNumber,
        'street' => $faker->streetAddress,
        'city' => $faker->city,
    ];
});

$factory->define(App\Models\Product::class, function (Faker $faker) {
    return [
        'name' => $faker->words(3, true),
        'price' => $faker->numberBetween(10000, 10000000),
        'requirements' => $faker->words(3, true),
        'ingredients' => $faker->paragraphs(3, true),
        'isValidate' => $faker->numberBetween(0,1),
        'description' => $faker->sentence,
        'stock' => $faker->numberBetween(0, 9999),
        'image' => 'https://loremflickr.com/400/300/pets?random=' . $faker->randomNumber(8, true),
        'url' => $faker->url,
        'ableToProduce' => null,
        'seller_id' => 1,
    ];
});

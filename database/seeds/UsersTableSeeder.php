<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 1)->create();
        factory(App\Models\Admin::class, 1)->create();
        factory(App\Models\Seller::class, 1)->create();
        factory(App\Models\Product::class, 25)->create();
    }
}

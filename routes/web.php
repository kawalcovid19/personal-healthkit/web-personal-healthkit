<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//Authentication User
$router->post("/register", "AuthController@register");
$router->post("/login", "AuthController@login");


//Authentication Seller
$router->post("/registerSeller", "SellerController@register");
$router->post("/loginSeller", "SellerController@login");


//Authentication Admin
$router->post("/registerAdmin", "AuthAdminController@register");
$router->post("/loginAdmin", "AuthAdminController@login");

//Admin
$router->put("/validateProduct/{id}", "AdminController@validateProduct");


$router->post('/products',[
    'uses' => 'ProductController@store',
    'middleware' => 'loginSeller'
]);

$router->get('/products', 'ProductController@index');
$router->get('/products/{id}', 'ProductController@show');
$router->post('/products/{id}',[
    'uses' => 'ProductController@update',
    'middleware' => 'loginSeller'
]);
$router->delete('/products/{id}',[
    'uses' => 'ProductController@delete',
    'middleware' => 'loginSeller'
]);
